[-> Etape d'installation](#installation)
# Pizza Fork

Dans ce TP, il était demandé de Construire une Application Web à l'aide du FrameWork CodeIgniter. Cela permettait de pouvoir gérer les Ingrédients, Garnitures et les Pizzas qu'il y avait dans la base de données. Qui ont été intégrer dans cette dernière à l'aide des Seeders.

# View
![image.png](./image.png)
![image-1.png](./image-1.png)
![image-2.png](./image-2.png)
![image-3.png](./image-3.png)
# Base de données

```plantuml 

class Pizza {
  * PK ID : INTEGER
  * text: VARCHAR [50]
  * picture: VARCHAR [50]

}
class Garniture {
  * PK ID :INTEGER
  * order:  VARCHAR [50]
  * quantiy: INTEGER

}
class Ingrédient {
  * PK ID :INTEGER
  * text:  VARCHAR [50]
  * picture: VARCHAR [50]

}

Pizza "1..1"--"1..*" Garniture 
Garniture "1..*"--"1..1" Ingrédient 

```

# Cas d'utilisation

```plantuml 
left to right direction
actor Administrateur as a
actor Client as c
actor Visiteur as v

package Site_Crystal_Pizza {
   usecase "Découvrir les pizzas" as UC1
  usecase "Ajouter des pizzas dans un panier" as UC2
  usecase "Se connecter" as UC3
  usecase "Ajouter des pizzas dans un panier" as UC4
  usecase "Payer les pizzas" as UC5
  usecase "Ajouter et créer des pizzas dans le catalogue" as UC6
  usecase "Ajouter des ingrédients dans le catalogues" as UC7
}
c -->UC1
c --> UC2
c --> UC3
v--|> c
v--> UC4
v--> UC5
a--|>v
a--> UC6
a--> UC7
```
# Exemple Code


Sauvegarde d'une pizza :

```
public function save(int $id = null) {
        $rules = $this->pizzaModel->getValidationRules();
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        } else {
            $form_data = [
                'text' => $this->request->getPost('text'),
            ];
            if (!is_null($id)) {
                $form_data['id'] = $id;
            }
            $pizza = new Pizza($form_data);
            $this->pizzaModel->save($pizza);
            return redirect()->to('/pizzas')->with('message', 'Pizza sauvegardée');
        }
    }


```

Sauvegarde d'une garniture :

```
public function save(int $id = null) {
        $rules = $this->garnitureModel->getValidationRules();
        if(!$this->validate($rules)){
            return redirect()->back()->withInput()->with('errors',$this->validator->getErrors());
        }
        else{
            $idPizza = $this->request->getPost('idIngredient');
            $form_data = [
                'idPizza' => $idPizza,
                'idIngredient' => $this->request->getPost('idIngredient'),
                'quantity' => $this->request->getPost('quantity'),
                'order' => $this->request->getPost('order'),
            ];
            if(!is_null($id)){
                $form_data['id'] = $id;
            }
            $garniture = new Garniture($form_data);
            $this->garnitureModel->save($garniture);
            return redirect()->to('/pizza/ingredients/'.$idPizza)->with('message','Garniture sauvegardée');
        }
    }

```

Sauvegarde d'un ingrédient :

```
public function save(int $id = null) {
        $rules = $this->ingredientModel->getValidationRules();
        $rules['picture']= [
            'upload[picture]',

        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        } else {
            $img = $this->request->getFile('picture');
            $img->move('img');
            $form_data = [
                'text' => $this->request->getPost('text'),
                'picture' => $img->getName(),
            ];
            if (!is_null($id)) {
                $form_data['id'] = $id;
            }
            $ingredient = new Ingredient($form_data);
            $this->ingredientModel->save($ingredient);            
            return redirect()->to('/ingredients')->with('message', 'Ingredient sauvegardé');
        }
    }

```


# Installation

## Etape 1 : Récupérer le dépot 
1.  git clone https://gitlab.com/Portfolio_sio2_jjr_jade_mourlon/projet-e5-jade-mourlon/codeingniter4.git
## Etape 2 : configurer l'environnement
Dans le terminal 
1. 
cd codeingniter4/
cd pizzaforkt/
2. composer update 
3. cp env .env
4. nano .env
5. Décommenter CI_ENVIRONMENT = development
et :
- database.default.hostname = localhost
- database.default.database = ci4
- database.default.username = root
- database.default.password = siojjr
- database.default.DBDriver = MySQLi
- database.default.DBPrefix =
## Etape 3 : Migrations et seeders
Dans le dossier pizzaforktp:

1. php spark db:create  ci4Auth
2. php spark migrate 
3. db:seed PizzaSeeder
4. db:seed IngredientSeeder
5. db:seed GarnitureSeeder
Dans la base de donnée:
- mysql -u root -p
- use ci4;
- insert into auth_groups values ( null ,'admin', 'Membres l administration du système de crystal Pizza');
- insert into auth_groups values ( null ,'user', 'Client  de crystal Pizza');
## Etape 4 : configurer le serveur mail
1. lancer mailDev
## Etape 5 :Créer un compte 
1. Lancer le serveur web : php spark serve
Lancer le serveur web : php spark serve
2. Créer et activer un compte (http://localhost:1080/ pour activer le compte) 
3. Sur dans le terminal :
- mysql -u root -p
- use ci4;
- select id from users where username= 'Votre username'; -- récupérer votre id 
- insert into auth_groups_users  values ( 1, 'Votre id');

Puis connecter vous à votre compte.
Pour tester le paiement :
Numéro de carte : 4242 4242 4242 4242 
