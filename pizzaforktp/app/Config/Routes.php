<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('PizzaController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'AccueilController::index');

$routes->get('/pizzas', 'PizzaController::index',['filter' => 'role:admin']);
$routes->get('/pizza/create', 'PizzaController::create',['filter' => 'role:admin']);
$routes->get('/pizza/delete/(:num)', 'PizzaController::delete/$1',['filter' => 'role:admin']);
$routes->post('/pizza/save', 'PizzaController::save',['filter' => 'role:admin']);
$routes->get('/pizza/edit/(:num)', 'PizzaController::edit/$1',['filter' => 'role:admin']);
$routes->post('/pizza/save/(:num)', 'PizzaController::save/$1',['filter' => 'role:admin']);
//
$routes->get('/ingredients', 'IngredientController::index');
$routes->get('/ingredient/create', 'IngredientController::create');
$routes->get('/ingredient/delete/(:num)', 'IngredientController::delete/$1');
$routes->post('/ingredient/save', 'IngredientController::save');
$routes->get('/ingredient/edit/(:num)', 'IngredientController::edit/$1');
$routes->post('/ingredient/save/(:num)', 'IngredientController::save/$1');
//
$routes->get('/pizza/ingredients/(:num)', 'GarnitureController::index/$1');
$routes->get('/pizza/ingredient/create/(:num)', 'GarnitureController::create/$1');
$routes->get('/pizza/ingredient/delete/(:num)', 'GarnitureController::delete/$1');
$routes->post('/pizza/ingredient/save', 'GarnitureController::save');
$routes->get('/pizza/ingredient/edit/(:num)', 'GarnitureController::edit/$1');
$routes->post('/pizza/ingredient/save/(:num)', 'GarnitureController::save/$1');

//route cart
$routes->get('/cart','CartController::index');
$routes->get('/ajoutCart/(:num)','CartController::add/$1');
$routes->get('/ajoutQty/(:any)','CartController::addQty/$1');
$routes->get('/suppQty/(:any)','CartController::removeQty/$1');
$routes->get('/suppQty/(:any)','CartController::removeQty/$1');
$routes->get('/suppLgn/(:any)','CartController::removeLine/$1');


//payement
$routes->get('/payer/(:num)','Payement::index/$1');
//$routes->get('/', 'HomeGuestController::index');
$routes->get('/profil/(:num)', 'HomeUserController::profil/$1'); 
$routes->get('/modifier/(:num)','HomeUserController::edit/$1');
$routes->post('/sauvegarder/(:num)', 'HomeUserController::save/$1');
$routes->get('/Home','HomeGuestController::index',['filter','MonFiltre']);
$routes->get('/guest','HomeGuestController::index');
$routes->group('',['filter' => 'login'], function($routes){
    $routes->get('/user','HomeUserController::index',['filter' => 'role:user']);
    $routes->get('/admin','HomeAdminController::index',['filter'=>'role:admin']);
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
