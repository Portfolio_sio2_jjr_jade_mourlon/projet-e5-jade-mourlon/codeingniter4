<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Faker\Factory;
use Faker\Generator;

class PizzaSeeder extends Seeder {
    public function run() {
        for ($i = 0; $i < 100; $i++) {
            $this->db->table('pizza')->insert($this->createPizza());
        }
    }

    private function createPizza(): array {
        /**
         * @var Generator $faker
         */
        $faker = Factory::create('it_IT');
        $city = $faker->city;
        $price = $faker->randomElement(['25', '21', '10', '9','20','15']);
        $img = $faker->randomElement(['1', '2', '3', '4','5','6','7','8']);
        return [
            'text' => $city, 'picture' => $img.".jpg", 'price' => $price,
        ];
    }
}
