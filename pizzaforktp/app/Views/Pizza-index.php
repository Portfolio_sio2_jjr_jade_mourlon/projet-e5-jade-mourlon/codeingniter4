<?= $this->extend('page.php') ?>
<?= $this->section('body') ?>
<div class="card">
    <div class="card-header">
        <h1><?= $title ?></h1>
    </div>
    <div class="container-fluid">
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="<?= base_url('/img/images/pexels-athena-2180877.jpg')?>" alt="First slide" height="400" >
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="<?= base_url('/img/images/pexels-kei-photo-2741457.jpg')?>" alt="Second slide" height="400">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="<?= base_url('/img/images/pexels-eneida-nieves-905847.jpg')?>" alt="Third slide"  height="400">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div> 
</div>
    <div class="card-body">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Pizza</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($pizzas as $pizza) : ?>
                    <tr>
                        <th scope="row"><?= $pizza->id ?></th>
                        <td><?= $pizza->text ?></td>
                        <td>
                            <a href="<?= '/pizza/edit/' . $pizza->id ?>" class="btn btn-primary" role="button">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="<?= '/pizza/ingredients/' . $pizza->id ?>" class="btn btn-secondary" role="button">
                                <i class="fas fa-list-ul"></i>
                            </a>
                            <a href="<?= '/pizza/delete/' . $pizza->id ?>" class="btn btn-danger" role="button">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
            
        </table>
        <?= $pager->links('default','bootstrapPager') ?>
    </div>
    
    <a class="btn btn-primary" href="pizza/create"><i class="fas fa-plus"></i></a>
</div>
<?= $this->endSection() ?>