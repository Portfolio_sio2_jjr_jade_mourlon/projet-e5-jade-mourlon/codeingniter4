<?= $this->extend('page.php') ?>
<?= $this->section('body') ?>

<div class="container-fluid">
  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="<?= base_url('/img/images/pexels-athena-2180877.jpg')?>" alt="First slide" height="400" >
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="<?= base_url('/img/images/pexels-kei-photo-2741457.jpg')?>" alt="Second slide" height="400">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="<?= base_url('/img/images/pexels-eneida-nieves-905847.jpg')?>" alt="Third slide"  height="400">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div> 
</div>
<div class="container-fluid">
<div class="card-columns">
  <?php 
    foreach( $pizzas as $pizza ){
      ?>
          <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="<?php echo base_url('/img/' . $pizza->picture)?>" alt=" <?php echo base_url('/img/' . $pizza->picture)?>" height="50%" >
        <div class="card-body">
          <h5 class="card-title"><?= $pizza->text ?></h5>
          <p class="card-text"><?= $pizza->price." €"?></p>
          <a href="<?= '/ajoutCart/'. $pizza->id ?>" class="btn btn-primary"><i class="fa-solid fa-basket-shopping"></i> Ajouter au panier </a>
        </div>
      </div>
      <?php
    }
  ?>
  </div>
</div>
<?= $pager->links('default','bootstrapPager') ?>
<?= $this->endSection() ?>
