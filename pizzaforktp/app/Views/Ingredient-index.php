<?= $this->extend('page.php') ?>
<?= $this->section('body') ?>
<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img class="d-block w-100" src="<?= base_url('/img/images/pexels-rafel-al-saadi-128536 (1).jpg')?>" alt="First slide" height="400px" >
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="<?= base_url('/img/images/pexels-tabitha-mort-773253.jpg')?>" alt="Second slide" height="400">
      </div>
      <div class="carousel-item">
        <img class="d-block w-100" src="<?= base_url('/img/images/pexels-polina-kovaleva-5645017.jpg')?>" alt="Third slide"  height="400">
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div> 
</div>
<div class="card">
    <div class="card-header">
        <h1><?=$title?></h1>
    </div>
    <div class="card-body">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Ingredient</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($ingredients as $ingredient) : ?>
                    <tr>
                        <th scope="row"><?= $ingredient->id ?></th>
                        <td><?= $ingredient->text ?></td>
                        <td>
                            <a href="<?= '/ingredient/edit/' . $ingredient->id ?>" class="btn btn-primary" role="button">
                                <i class="fas fa-edit"></i>
                            </a>
                            <a href="<?= '/ingredient/delete/' . $ingredient->id ?>" class="btn btn-danger" role="button">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
    </div>
    <a class="btn btn-primary" href="/ingredient/create"><i class="fas fa-plus"></i></a>
</div>
<?= $this->endSection() ?>