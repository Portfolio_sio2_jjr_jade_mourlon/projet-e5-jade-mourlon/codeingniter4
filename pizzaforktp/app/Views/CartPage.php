<?php use CodeIgniterCart\Cart ?>
<?= $this->extend('page.php') ?>
<?= $this->section('body') ?>
<?php $cart = \Config\Services::cart();?>
<?php echo form_open('path/to/controller/update/method'); ?>
<table cellpadding="6" cellspacing="1" style="width:100%" border="0">
<tr>
        <th>QTY</th>
        <th>Item Description</th>
        <th >Item Price</th>
        <th >Sub-Total</th>
        <th >   </th>
        <th >   </th>
</tr>
<?php $i = 1;
$TotalPizza=0.0;
 ?>
<?php 
foreach ($cart->contents() as $items):
        echo "<tr><td>".$items['qty']."</td>";
        echo "<td>".$items['name']."</td>";
        echo "<td>".$items['price']."</td>";
       $tx=$items['price']*$items['qty'];
       echo "<td>".$tx."</td>";
       echo '<td><a class="btn btn-danger" href="/ajoutQty/'.$items['rowid'].'" role="button"><i class="fa-solid fa-plus"></i></a></td>';
       echo '<td><a class="btn btn btn-info" href="/suppQty/'.$items['rowid'].'" role="button"><i class="fa-solid fa-minus"></i></a></td>';
       echo '<td><a class="btn btn-dark" href="/suppLgn/'.$items['rowid'].'" role="button"><i class="fa-solid fa-ban"></i></a></td></tr>';
       $TotalPizza=$TotalPizza+$tx;
         ?>      
<?php endforeach; ?>
</table>
</form>

  <?php

if($cart->totalItems()==0){
  
} else { ?>
<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">Montant de votre commande : </h5>
        <p class="card-text"><?php echo "Montant : " . $TotalPizza . " € " ;?></p>
        <!--<a href="/payer/" class="btn btn-primary">Payer</a>-->
<?php 
if (isset(user()->username)){
?>
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter"><i class="fa-solid fa-credit-card"></i>
  Payer
</button>
<?php
}else { ?>
<a href="<?='/login'?>"  >
  Se connecter
</button>
<?php
} ?>
<?php } ?>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Informations bancaire : </h5>
      </div>
      <div class="modal-body">
      <form   action=<?= "/payer/".$TotalPizza ?>  >
        <div class="form-row">
        <div class="form-group col-md-6">
          <label for="name">Votre Nom : </label>
          <input type="text" class="form-control" id="name" required  placeholder="Nom" data-stripe="">
        </div>
        <div class="form-group col-md-6">
          <label for="email">Votre addresse mail : </label>
          <input type="email" class="form-control" id="email" required placeholder="votre@email.fr" data-stripe="">
          </div>
        
        </div>
        <div class="form-group">
        <label for="inputAddress">Numéro de carte : </label>
        <input type="text" class="form-control" id="numCarte" required  placeholder="Votre code de carte bleu" data-stripe="card-number">
        </div>
        <!--<div class="form-group">
        <label for="inputPassword4">Password</label>
          <input type="password" class="form-control" id="inputPassword4" placeholder="******">
        </div>-->
        <div class="form-row">
          <div class="form-group col-md-2">
            <label for="month">MM</label>
            <input type="text" class="form-control" id="month" data-stripe="card-expiry-month">
          </div>
          /
          <div class="form-group col-md-2">
            <label for="years">YY</label>
            <input type="text" class="form-control" id="years" data-stripe="card-expiry-year">
              
          </div>
          <div class="form-group col-md-2">
            <label for="cvc">CVC</label>
            <input type="text" class="form-control" id="cvc" data-stripe="card-cvc">
          </div>
        </div>
        <div class="form-group">
          <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
              Accepter le CGU
            </label>
          </div>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
              Accepter le CGV
            </label>

        </div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa-solid fa-xmark"> </i> Fermer</button>
        <div id="payment-element">
        <!--Stripe.js injects the Payment Element-->
      </div>
      <button id="submit">
        <div class="spinner hidden" id="spinner"></div>
        <span id="button-text">Payer Maintenant</span>
      </button>
      <div id="payment-message" class="hidden"></div>
      </div>
      </form>
    </div>
  </div>
</div>
      </div>
    </div>
  </div>
</div>
<p><?php //echo form_submit('', 'Update your Cart'); ?></p>
<?= $this->endSection() ?>
