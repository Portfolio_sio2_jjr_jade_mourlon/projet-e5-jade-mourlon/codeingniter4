
<?= $this->extend('page.php')?>
<?= $this->section('body')?>

<form class"form-horizontal" action="<?= (isset(user()->id) ? '/sauvegarder/'.user()->id : '/sauvegarder' )?>" method="post">
    <?= csrf_field() ?>
    <div class="form-group">
        <label for="username"><?=lang('Auth.username')?></label>
        <input type="text" class="form-control <?php if(session('errors.username')) : ?>is-invalid<?php endif ?>" name="username" placeholder="<?= user()->username ?>" value="<?= user()->username ?>">
    </div>
    
    <div class="form-group">
        <label for="email"><?=lang('Auth.email')?></label>
        <input type="email" class="form-control <?php if(session('errors.email')) : ?>is-invalid<?php endif ?>"
               name="email" aria-describedby="emailHelp" placeholder="<?=lang('Auth.email')?>" value="<?= user()->email ?>">
        <small id="emailHelp" class="form-text text-muted"><?=lang('Auth.weNeverShare')?></small>
    </div>
    <div class="form-group">
        <label for="password">Nouveau Mot de passe</label>
        <input type="password" name="password" class="form-control <?php if(session('errors.password')) : ?>is-invalid<?php endif ?>" placeholder="*******" autocomplete="off">
    </div>
    <div class="form-group">
        <label for="pass_confirm">Répéter le nouveau mot de passe</label>
        <input type="password" name="pass_confirm" class="form-control <?php if(session('errors.pass_confirm')) : ?>is-invalid<?php endif ?>" placeholder="<?=lang('Auth.repeatPassword')?>" autocomplete="off">
    </div>
    <br>
    <button type="submit" class="btn btn-primary btn-block"><i class="fa-solid fa-circle-plus">Valider</i></button>
</form>
<?= $this->endSection()?>