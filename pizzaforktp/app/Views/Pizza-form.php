<?= $this->extend('page.php') ?>
<?= $this->section('body') ?>
<div class="card">
    <div class="card-header">
        <h1><?= $title ?></h1>
    </div>
    <div class="card-body">
        <?= ((session()->has('errors')) ? \Config\Services::validation()->listErrors() : '') ?>
        <form class="form-horizontal" action="<?= (isset($pizza) ? '/pizza/save/' . $pizza->id : '/pizza/save') ?>" method="post" enctype="multipart/form-data">
            <div class=form-group>
                <form-label for="text ">Pizza : </form-label>
                <input type="text" name="text" id="text" value="<?= old('text', $pizza->text ?? '', false) ?>" placeholder="Nom de la pizza">
            </div>
            <div class=form-group>
                <label for="fileName ">Image</label>
                <input type="text" name="fileName" id="fileName" class="form-control" value="<?= old('picture', $pizza->picture ?? '', false) ?>" disabled>
            </div>
            <div class=form-group>
                <input type="button" id="button" value="Choisir un fichier" class="btn btn-secondary" onclick="document.getElementById('picture').click()"/>
                <input type="file" name="picture" id="picture" style="display:none" onchange="previewPicture(this)" />
            </div>
            <?php if(isset($pizza->picture)){ ?>
                <div class=form-group >
                <img src="<?= old('picture', base_url('/img/'. $pizza->picture) ?? '', false) ?>" class ="img-fluid-thumbnail" width="200" height="200">
            </div>
            <?php
            }else{
                ?>
                
                <img src="#" alt="" id="image" style="max-width: 500px; margin-top: 20px;" >
            <?php }?>
            <button class="btn btn-primary" type="submit">
            </br>
                <i class="fa fa-plus"> Valider</i>
            </button>
        </form>


    

   

</form>



    </div>
</div>

<script type="text/javascript" >
    // L'image img#image
    var image = document.getElementById("image");
     
    // La fonction previewPicture
    var previewPicture  = function (e) {

        // e.files contient un objet FileList
        const [picture] = e.files

        // "picture" est un objet File
        if (picture) {
            // On change l'URL de l'image
            image.src = URL.createObjectURL(picture)
        }
    } 
</script>

<?= $this->endSection() ?>