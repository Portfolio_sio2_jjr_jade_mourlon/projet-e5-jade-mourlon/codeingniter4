<?php namespace App\Libraries;

use App\Controllers\BaseController;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;
use Myth\Auth\Authorization\GroupModel;
class AdminVerif{

    public static function adminVerif(){
        helper('auth');
        if(isset(user()->id)){
            $group= new GroupModel();
           $grouplist =$group->getGroupsForUser(user()->id);
           return $grouplist[0];
           //throw new \Exception($grouplist[0]);
        }
        
    }
}
