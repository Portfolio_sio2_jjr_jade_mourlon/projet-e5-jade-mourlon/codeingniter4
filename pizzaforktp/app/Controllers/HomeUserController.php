<?php

namespace App\Controllers;
use App\Controllers\BaseController;
use App\Controllers\HomeUserController;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;
use Myth\Auth\Authorization\GroupModel;
use App\Libraries\AdminVerif;
class HomeUserController extends BaseController
{
	public function __construct(){
		$this->userModel=new UserModel();
		$this->groupModel= new GroupModel();
	}
    public function index()
    {
        $data['title']="slam:Auth";
        return view('HomeUser',$data);
    }
	
    public function profil(int $id)
    {
		$data['role'] =  AdminVerif::adminVerif();;
		$user=$this->userModel->find($id);
		$data['user']=$user;
        $data['title']="Profil de ".user()->username;
        return view('profil.php',$data);
    }

   
    public function save(int $id)
	{
		$data['role'] =  AdminVerif::adminVerif();;
		$users = model(UserModel::class);

		// Vérification des données saisie par l'utilisateur sinon redirection
		$rules = [
			'username' => 'required|alpha_numeric_space|min_length[3]|max_length[30]|is_unique[users.username]',
			'email'    => 'required|valid_email|is_unique[users.email]',
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

		
		$rules = [
			'password'     => 'required|strong_password',
			'pass_confirm' => 'required|matches[password]',
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

		// Save the user
		$allowedPostFields = array_merge(['password'], $this->config->validFields, $this->config->personalFields);
		$user = new User($this->request->getPost($allowedPostFields));
		 
		$this->config->requireActivation === null ? $user->activate() : $user->generateActivateHash();

		// Ensure default group gets assigned if set
        if (! empty($this->config->defaultUserGroup)) {
            $users = $users->withGroup($this->config->defaultUserGroup);
        }

		if (! $users->save($user))
		{
			return redirect()->back()->withInput()->with('errors', $users->errors());
		}

		if ($this->config->requireActivation !== null)
		{
			$activator = service('activator');
			$sent = $activator->send($user);

			if (! $sent)
			{
				return redirect()->back()->withInput()->with('error', $activator->error() ?? lang('Auth.unknownError'));
			}

			// Success!
			return redirect()->route('login')->with('message', lang('Auth.activationSuccess'));
		}

		// Success!
		return redirect()->route('login')->with('message', lang('Auth.registerSuccess'));
	}
	
}
