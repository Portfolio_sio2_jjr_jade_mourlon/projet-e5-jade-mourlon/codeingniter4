<?php

namespace App\Controllers;
use Myth\Auth\Entities\User;
use App\Controllers\BaseController;
use App\Models\PizzaModel;
use App\Entities\Pizza;
use App\Models\GarnitureModel;
use App\Entities\Ingredient;
use Myth\Auth\Models\UserModel;
use Myth\Auth\Authorization\GroupModel;
use Omnipay\Omnipay;

use App\Libraries\AdminVerif;

class CartController extends BaseController {
    /** @var PizzaModel $pizzaModel */
    protected $pizzaModel;
    protected $cart;
    // URL Helpers : aident à créer des liens
    //Form Helpers : aident à créer des éléments de formulaire
    public function __construct() {
        $this->helpers = ['form', 'url'];
        $this->pizzaModel = new PizzaModel();
        $cart = \Config\Services::cart();
        $this->userModel=new UserModel();
        $this->groupModel= new GroupModel();
           
    }
    // renvoie la page Cartpage.php
    public function index() {
        $data['pizzas'] = $this->pizzaModel->getAll();
        $data['title'] = "Mon Panier" ;
        $data['pager'] = $this->pizzaModel->pager;
        $data['role'] =  AdminVerif::adminVerif();;
        $cart = \Config\Services::cart();
        
        return view('CartPage.php', $data);
    }
    //permet d'ajouter une pizza au panier
    public function add(int $id) {
        $data['pizza'] = $this->pizzaModel->getById($id);
        $pizza=$this->pizzaModel->getById($id);
        $data['title'] = "Mon Panier" ;
        $data['pager'] = $this->pizzaModel->pager;
        $data['role'] =  AdminVerif::adminVerif();;
        $cart = \Config\Services::cart();
        $data['pizzas'] = $this->pizzaModel->getAll();
        
    $cart ->insert(array(
        'id'      => $pizza->id,
        'qty'     => 1,
        'price'   => $pizza->price,
        'name'    => addslashes($pizza->text),
        
     ));
    
    
        return view('CartPage.php', $data);
    }
    // permet d'augmenter la quantité d'une pizza déja présente de la panier
    public function addQty(string $rowid){
        $cart = \Config\Services::cart();
       foreach ($cart->contents() as $items):
        if($items['rowid']==$rowid){
            $a=$items['qty'];
        }     
        endforeach; 
        $cart->update(array(
            'rowid'   => $rowid,
            'qty'     => $a+1, 
         ));
         $data['title'] = "Mon Panier";
         return view('CartPage.php', $data);
    }
     //Permet de réduire la quantité d'un pizza déjà présente dans le panier et efface la pizza si la quantité arrive à 0 
    public function removeQty(string $rowid){
        $cart = \Config\Services::cart();
       foreach ($cart->contents() as $items):
        if($items['rowid']==$rowid){
            $a=$items['qty'];
        }     
        endforeach; 
        $cart->update(array(
            'rowid'   => $rowid,
            'qty'     => $a-1, 
         ));
         $data['title'] = "Mon Panier";
        foreach ($cart->contents() as $items):
           if($items['qty']==0){
            $cart->remove($rowid);
           } 
        endforeach;
        return view('CartPage.php', $data);
    }
     //Permet de supprimer une pizza 
    public function removeLine(string $rowid){
        $cart = \Config\Services::cart();
        $data['title'] = "Mon Panier";
        $cart->remove($rowid);
        return view('CartPage.php', $data);
    }

   
 
}
