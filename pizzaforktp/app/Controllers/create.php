<?php

require 'vendor/autoload.php';

// Ceci est votre clé API secrète de test.
\Stripe\Stripe::setApiKey('sk_test_51Ko15aCUs7G9vPBgmKyj7BhQsno6Pl4JTl3g4Xur7QGzOQfsL2e1QCKxGe1Bkc8mIYUmLeE4jWFdLxgz99waCdds00pSY7rypz');

function calculateOrderAmount(array $items): int {
    // Remplace cette constante par un calcul du montant de la commande
    // Calcule le total de la commande sur le serveur pour éviter
    // personnes de manipuler directement le montant sur le client
    return 1400;
}

header('Content-Type: application/json');

try {
    // récupère le JSON du corps POST
    $jsonStr = file_get_contents('php://input');
    $jsonObj = json_decode($jsonStr);

   // Créer un PaymentIntent avec montant et devise
    $paymentIntent = \Stripe\PaymentIntent::create([
        'amount' => calculateOrderAmount($jsonObj->items),
        'currency' => 'eur',
        'automatic_payment_methods' => [
            'enabled' => true,
        ],
    ]);

    $output = [
        'clientSecret' => $paymentIntent->client_secret,
    ];

    echo json_encode($output);
} catch (Error $e) {
    http_response_code(500);
    echo json_encode(['error' => $e->getMessage()]);
}