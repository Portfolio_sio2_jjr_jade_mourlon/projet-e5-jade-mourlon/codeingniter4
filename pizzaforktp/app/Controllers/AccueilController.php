<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PizzaModel;
use App\Entities\Pizza;
use Myth\Auth\Entities\User;
use Myth\Auth\Models\UserModel;
use Myth\Auth\Authorization\GroupModel;
use App\Libraries\AdminVerif;

class AccueilController extends BaseController {
    /** @var PizzaModel $pizzaModel */
    protected $pizzaModel;
    // URL Helpers : aident à créer des liens
    //Form Helpers : aident à créer des éléments de formulaire
    public function __construct() {
        $this->helpers = ['form', 'url'];
        $this->userModel=new UserModel();
        $this->groupModel= new GroupModel();
        $this->pizzaModel = new PizzaModel();

        $adminVerif= AdminVerif::adminVerif();
           
    }
    // renvoie sur la page accueil 
    public function index() {
        $data['pizzas'] = $this->pizzaModel->getAll();
        $data['title'] = "Crystal Pizza";
        $data['pager'] = $this->pizzaModel->pager;
        $data['pizzas'] = $this->pizzaModel->getAll();
        $data['role'] =  AdminVerif::adminVerif();;
        
        return view('accueil.php', $data);
    }
    


   


    
}
