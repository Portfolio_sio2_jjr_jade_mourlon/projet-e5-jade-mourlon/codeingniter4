<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\PizzaModel;
use App\Entities\Pizza;
use App\Models\GarnitureModel;
use App\Entities\Ingredient;
use Myth\Auth\Authorization\GroupModel;
use App\Libraries\AdminVerif;
class PizzaController extends BaseController {
    /** @var PizzaModel $pizzaModel */
    protected $pizzaModel;

    public function __construct() {
        $this->helpers = ['form', 'url'];
        $this->pizzaModel = new PizzaModel();
        $this->groupModel= new GroupModel();
        
    }
    //Renvoie sur la page Pizza-index.php
    public function index() {
        $data['pizzas'] = $this->pizzaModel->getAll();
        $data['title'] = "Les pizzas";
        $data['pager'] = $this->pizzaModel->pager;
        $data['role'] =  AdminVerif::adminVerif();;
        return view('Pizza-index.php', $data);
    }
    // Renvoie sur la page Pizza-form.php
    public function create() {
        $data['title'] = 'Nouvelle Pizza';
        $data['role'] =  AdminVerif::adminVerif();;
        return view('Pizza-form.php', $data);
    }
    //Supprime une pizza
    public function delete(int $id) {
        $this->pizzaModel->delete($id);
        $data['role'] =  AdminVerif::adminVerif();;
        return redirect()->to('/pizzas')->with('message', 'Pizza supprimée');
    }

    public function save(int $id = null) {
        $rules = $this->pizzaModel->getValidationRules();
        //controle lier aux images
        $rules['picture'] = [
            'uploaded[picture]',
            'mime_in[picture,image/jpg,image/jpeg,image/png]',
            'max_size[picture,1024]',
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        } else {
            //ajout des images
            $img =$this->request->getFile('picture');
            $img->move('img');
            $form_data = [
                'text' => $this->request->getPost('text'),
                'picture' => $img->getName(),
            ];
            if (!is_null($id)) {
                $form_data['id'] = $id;
            }
            $pizza = new Pizza($form_data);
            $this->pizzaModel->save($pizza);
            $data['role'] =  AdminVerif::adminVerif();;
            return redirect()->to('/pizzas')->with('message', 'Pizza sauvegardée');
        }
    }

    public function edit(int $id) {
        $data['title'] = "Modifier pizza";
        $data['pizza'] = $this->pizzaModel->find($id);
        $data['role'] =  AdminVerif::adminVerif();;
        return view('Pizza-form.php', $data);
    }
  
}
