<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\IngredientModel;
use App\Models\GarnitureModel;
use App\Entities\Ingredient;
use Myth\Auth\Authorization\GroupModel;
use App\Libraries\AdminVerif;

class IngredientController extends BaseController {
    /** @var IngredientModel $ingredientModel */
    protected $ingredientModel;

    public function __construct() {
        $this->helpers = ['form', 'url'];
        $this->ingredientModel = new IngredientModel();
        $this->groupModel= new GroupModel();
    }
    //Revoie sur la page
    public function index() {
        $ingredients = $this->ingredientModel->orderBy('id')->findAll();
        $data['ingredients'] = $ingredients;
        $data['title'] = "Les ingredients";
        $data['role'] =  AdminVerif::adminVerif();;
        return view('Ingredient-index.php', $data);
    }
    //Revoie sur la page
    public function create() {
        $data['title'] = 'Nouvel Ingredient';
        $data['role'] =  AdminVerif::adminVerif();;
        return view('Ingredient-form.php', $data);
    }
    //Supprime un ingrédient de la garniture 
    public function delete(int $id) {
        $LesGarnitures= new GarnitureModel;
        $data['role'] =  AdminVerif::adminVerif();;
        $SuppValid=$LesGarnitures->SuppValid($id);
        if($SuppValid==false){
            $this->ingredientModel->where(['id' => $id])->delete();
            return redirect()->to('/ingredients')->with('message', 'Ingredient supprimé');
        }else{
            return redirect()->to('/ingredients')->with('message', 'L\'ingrédient ne peut être supprimé');
        }
        
    }
     // fonction de save des données de l'ingrédient
    public function save(int $id = null) {
        $data['role'] =  AdminVerif::adminVerif();;
        $rules = $this->ingredientModel->getValidationRules();
        $rules['picture'] = [
            'uploaded[picture]',
            'mime_in[picture,image/jpg,image/jpeg,image/png]',
            'max_size[picture,1024]',
        ];
        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
        } else {
            //upload de l'image
            $img =$this->request->getFile('picture');
            $img->move('img');
            $form_data = [
                'text' => $this->request->getPost('text'),
                'picture' => $img->getName(),
            ];
            if (!is_null($id)) {
                $form_data['id'] = $id;
            }
            $ingredient = new Ingredient($form_data);
            $this->ingredientModel->save($ingredient);            
            return redirect()->to('/ingredients')->with('message', 'Ingredient sauvegardé');
        }
    }

    public function edit(int $id) {
        $data['role'] =  AdminVerif::adminVerif();;
        $data['title'] = "Modifier ingredient";
        $data['ingredient'] = $this->ingredientModel->find($id);
        return view('Ingredient-form.php', $data);
    }

}

