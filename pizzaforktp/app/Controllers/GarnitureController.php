<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\GarnitureModel;
use App\Entities\Garniture;
use App\Models\IngredientModel;
use App\Models\PizzaModel;
use Myth\Auth\Authorization\GroupModel;
use App\Libraries\AdminVerif;
class GarnitureController extends BaseController {
    /** @var GarnitureModel $garnitureModel */
    protected $garnitureModel;
    protected $pizzaModel;

    public function __construct() {
        $this->helpers = ['form', 'url'];
        $this->garnitureModel = new GarnitureModel();
        $this->pizzaModel = new PizzaModel();
        $this->groupModel= new GroupModel();
    }
    //Renvoie sur la page Garniture-index.php
    public function index($idPizza) {
        $data['role'] =  AdminVerif::adminVerif();;
        $data['pizza'] = $this->pizzaModel->getById($idPizza);
        $data['title'] = "Les garnitures";
        
        return view('Garniture-index.php', $data);
    }
    // permet de créer une nouvelle garniture
    public function create(int $idPizza) {
        $data['role'] =  AdminVerif::adminVerif();;
        $ingredientModel = new IngredientModel();
        $data['idPizza'] = $idPizza;
        $data['title'] = 'Nouvelle Garniture';
        $data['ingredients'] = $ingredientModel->findAll();
        return view('Garniture-form.php', $data);
    }
    //permet de supprimer un ingrédient de la garniture d'une pizza ( la pate à pizza ne peut etre supprimée)
    public function delete(int $id) {
        $data['role'] =  AdminVerif::adminVerif();;
        $idIngredient = $this->garnitureModel->find($id)->idIngredient;
        $configPizzaFork = config('PizzaFork');
        $idPate = $configPizzaFork->idPateAPain;
        if($idIngredient==$idPate){
            $idPizza = $this->garnitureModel->find($id)->idPizza;
            return redirect()->to('pizza/ingredients/'.$idPizza)->with('message', 'La pâte à pizza ne peut être supprimée');
        } else {
            $idPizza = $this->garnitureModel->find($id)->idPizza;
            $this->garnitureModel->delete($id);
            return redirect()->to('pizza/ingredients/'.$idPizza)->with('message', 'Garniture supprimée');
        }
        
    }
    // Permet de save une garniture
    public function save(int $id = null) {
        $data['role'] =  AdminVerif::adminVerif();;
        $rules = $this->garnitureModel->getValidationRules();
        if(!$this->validate($rules)){
            return redirect()->back()->withInput()->with('errors',$this->validator->getErrors());
        }
        else{
            $idPizza = $this->request->getPost('idPizza');
            $form_data = [
                'idPizza' => $idPizza,
                'idIngredient' => $this->request->getPost('idIngredient'),
                'quantity' => $this->request->getPost('quantity'),
                'order' => $this->request->getPost('order'),
            ];
            if(!is_null($id)){
                $form_data['id'] = $id;
            }
            $garniture = new Garniture($form_data);
            $this->garnitureModel->save($garniture);
            return redirect()->to('/pizza/ingredients/'.$idPizza)->with('message','Garniture sauvegardée');
        }
    }
    //Renvoie sur la page de modification d'une garniture
    public function edit(int $id) {
        $data['role'] =  AdminVerif::adminVerif();;
        $ingredientModel = new IngredientModel();
        $data['title'] = "Modifier une garniture";
        $data['garniture'] = $this->garnitureModel->getById($id);
        $data['ingredients'] = $ingredientModel->findAll();
        return view('Garniture-form.php',$data);
    }
  
}
